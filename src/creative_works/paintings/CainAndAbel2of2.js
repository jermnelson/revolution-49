import React from "react"

import CainAndAbel2of2Image001 from "../../images/timothy_semple/CainAndAbel2of2Image001.png"

const CainAndAbel20f2 = () => (
  <div className="painting">
    <h4>Part of the <em>Soli Deo Glora!</em> Series</h4>
    <h3>Cain and Abel (2 of 2)</h3>
    <img src={CainAndAbel2of2Image001} alt="Cain and Abel 2 of 2" />
    <div>
      Oil on canvas, 101 cm by 66 cm. 2013. $1000.00 (must be purchased as a pair)
    </div>
  </div>
)

export default CainAndAbel20f2
