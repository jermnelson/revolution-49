import React from "react"

import ColorWheelImage001 from "../../images/timothy_semple/ColorWheelImage001.png"

const ColorWheel = () => (
  <div className="painting">
    <h4>Part of the <em>Soli Deo Glora!</em> series</h4>
    <h3>Color Wheel</h3>
    <img src={ColorWheelImage001} alt="Color Wheel 2013" />
    <div>
      Oil on canvas, 35 cm by 28 cm. 2013. $300.00
    </div>
  </div>
)

export default ColorWheel
