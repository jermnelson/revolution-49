import React from "react"

import IntheGardenIIIImage001 from "../../images/timothy_semple/IntheGardenIIIImage001.png"

const InTheGardenIII = () => (
  <div className="painting">
    <h4>Part of the <em>Soli Deo Glora!</em> series</h4>
    <h3>In the Garden III</h3>
    <img src={IntheGardenIIIImage001} alt="In the Garden III 2013" />
    <div>
     Oil on canvas, 60 cm by 45 cm. 2013. $850.00
    </div>
  </div>
)

export default InTheGardenIII
