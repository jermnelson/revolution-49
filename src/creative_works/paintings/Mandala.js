import React from "react"

import MandalaImage001 from "../../images/timothy_semple/MandalaImage001.png"

const Mandala = () => (
  <div className="painting">
    <h4>Part of the <em>Soli Deo Glora!</em> series</h4>
    <h3>Mandala</h3>
    <img src={MandalaImage001} alt="Mandala 2013" />
    <div>
      Oil on canvas, 28 cm by 35 cm. 2013 $300.00
    </div>
  </div>
)

export default Mandala
