import React from "react"
import ArtForHumansImage001 from "../../images/timothy_semple/ArtForHumansImage001.png"

const ArtForHumans = () => (
  <div className="painting">
    <h4>Part of the <em>Soli Deo Glora!</em> series</h4>
    <h3>Art for Humans</h3>
    <img src={ArtForHumansImage001} alt="Art for Humans" />
    <div>
      Oil on canvas, 50cm by 40cm. 2013. $500.00
    </div>
  </div>
)

export default ArtForHumans
