import React from "react"

import IriminageImage001 from "../../images/jeremy_nelson/IriminageImage001.png"

const Iriminage = () => (
  <div className="painting">
   <h4><em>Aiki</em> Series</h4>
   <h3>Iriminage</h3>
   
   <img src={IriminageImage001} alt="Iriminage 2012" />
   <div>
    Oil on canvas, 50 cm by 40 cm. 2012 $550.00
   </div>
  </div>
)
export default Iriminage
