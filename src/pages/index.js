import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>An Artist Collective</h1>
    <p>
      <Link to="/artists/jeremy_nelson">Jeremy Nelson</Link>
      , <Link to="/artists/jake_nelson">Jake Nelson</Link> 
      , <Link to="/artists/carmen_allen">Carmen Allen</Link>, 
      and <Link to="/artists/timothy_semple">Timothy Semple</Link>
    </p>
    <p>Who we support? People</p>
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
)

export default IndexPage
