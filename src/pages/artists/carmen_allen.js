import React from "react"
import { Link } from "gatsby"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

const CarmenAllen = () => {
  return (<Layout>
    <SEO title="Carmen Allen artistic works" author="Carmen Allen" />
    <h1>Carmen Allen</h1>
    <div className="gallery">
    </div>
  </Layout>)
}

export default CarmenAllen 
