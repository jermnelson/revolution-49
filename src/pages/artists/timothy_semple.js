import React from "react"
import { Link } from "gatsby"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

// Paintings
import ArtForHumans from "../../creative_works/paintings/ArtForHumans"
import CainAndAbel2of2 from "../../creative_works/paintings/CainAndAbel2of2"
import ColorWheel from "../../creative_works/paintings/ColorWheel"
import InTheGardenIII from "../../creative_works/paintings/InTheGardenIII"
import Mandala from "../../creative_works/paintings/Mandala"

const  TimothySemple = () => {

  return (<Layout>
    <SEO title="Timothy Semple artistic works" author="Jeremy Nelson" />
    <h1>Timothy Semple</h1>
    <div className="gallery">
      <ArtForHumans />
      <CainAndAbel2of2 />
      <ColorWheel />
    </div>
    <hr />
    <div className="gallery">
      <InTheGardenIII />
      <Mandala />
    </div>
  </Layout>)

}

export default TimothySemple
