import React from "react"
import { Link } from "gatsby"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

// Paintings
import Irimnage from "../../creative_works/paintings/Iriminage"

const JeremyNelson = () => {

  return (<Layout>
    <SEO title="Jeremy Nelson artistic works" author="Jeremy Nelson" />
    <h1>Jeremy Nelson</h1>
    <div className="gallery">
      <Irimnage />
    </div>
  </Layout>)

}

export default JeremyNelson
