import React from "react"
import { Link } from "gatsby"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

const JakeNelson = () => {
  return (<Layout>
    <SEO title="Jake Nelson artistic works" author="Jake Nelson" />
    <h1>Jake Nelson</h1>
    <div className="gallery">
    </div>
  </Layout>)
}

export default JakeNelson
